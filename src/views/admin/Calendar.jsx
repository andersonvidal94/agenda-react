import React from "react";
import BigCalendar from "react-big-calendar";
import moment from "moment";
import "moment/locale/es";
//require('globalize/lib/cultures/globalize.culture.es')
import "react-big-calendar/lib/css/react-big-calendar.css";
import { events, authRef } from "../../config/firebase";
import Form from "../../components/Form";
import { appointment } from "../../config/structs";
import Event from "../../models/Event";
// material-ui
import { withStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";

const styles = theme => ({
	modal: {
		top: 50,
		left: 50,
		transform: `translate(-50%, -50%)`,
	},
	fab: {
		position: "absolute",
		bottom: theme.spacing.unit * 2,
		right: theme.spacing.unit * 2,
	},
	paper: {
		position: "absolute",
		width: theme.spacing.unit * 50,
		backgroundColor: theme.palette.background.paper,
		boxShadow: theme.shadows[5],
		padding: theme.spacing.unit * 4,
		outline: "none",
		top: "50%",
		left: "50%",
		transform: `translate(-50%, -50%)`,
	},
});

const localizer = BigCalendar.momentLocalizer(moment);
let allViews = Object.keys(BigCalendar.Views).map(k => BigCalendar.Views[k]);
const minTime = new Date();
minTime.setHours(8, 0, 0);
const maxTime = new Date();
maxTime.setHours(16, 0, 0);

class Calendar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			events: [
			],
			event: { ...Event, created_by: authRef.currentUser.uid },
			dayChosen: props.date ? new Date(props.date + " 00:00:00") : new Date(),
			minTime: minTime,
			maxTime: maxTime,
			openEventModal: false,
		};
		this.query = this.props.query || events.where("date", ">=", this.state.dayChosen.toISOString().split("T")[0])
	}

	componentDidMount() {	
		const self = this;
		const items = [...this.state.events];
		this.query.onSnapshot(snapshot => {
			snapshot.docChanges().forEach(change => {
				const data = { ...change.doc.data(), id: change.doc.id };
				data.start = new Date(data.start)
				data.end = new Date(data.end)
				if (change.type === "added") {
					items.push(data);
				}
				if (change.type === "modified") {
					let found = items.findIndex(e => {
						return e.hc === change.doc.hc;
					});
					items[found] = data;
				}
				if (change.type === "removed") {
					let found = items.findIndex(e => {
						return e.hc === change.doc.hc;
					});
					items.splice(found, 1);
				}
			});
			self.setState({ events: items });
			console.log(items);
		});
	}
	componentWillUnmount() {
		const unsuscribe = this.query
			.onSnapshot(() => {});
		unsuscribe();
	}
	onNavigate = (focusDate, flipUnit, prevOrNext) => {
		console.log(
			"what are the 3 params focusDate, flipUnit, prevOrNext ?",
			focusDate,
			flipUnit,
			prevOrNext,
		);
		const _this = this;
		const now = new Date();
		const nowNum = now.getDate();
		const maxDateAvailable = moment()
			.add(2, "month")
			.toDate();

		if (flipUnit === "month") {
			if (prevOrNext === "NEXT" && _this.state.dayChosen < maxDateAvailable) {
				_this.setState({
					dayChosen: moment(_this.state.dayChosen)
						.add(1, "month")
						.toDate(),
				});
			} else if (prevOrNext === "PREV") {
				_this.setState({
					dayChosen: moment(_this.state.dayChosen)
						.subtract(1, "month")
						.toDate(),
				});
			}
		} else if (flipUnit === "week") {
			if (prevOrNext === "NEXT" && _this.state.dayChosen <= maxDateAvailable) {
				_this.setState({
					dayChosen: moment(_this.state.dayChosen)
						.add(7, "day")
						.toDate(),
				});
			} else if (prevOrNext === "PREV") {
				_this.setState({
					dayChosen: moment(_this.state.dayChosen)
						.subtract(7, "day")
						.toDate(),
				});
			}
		} else if (flipUnit === "work_week") {
			if (prevOrNext === "NEXT" && _this.state.dayChosen <= maxDateAvailable) {
				_this.setState({
					dayChosen: moment(_this.state.dayChosen)
						.add(5, "day")
						.toDate(),
				});
			} else if (prevOrNext === "PREV") {
				_this.setState({
					dayChosen: moment(_this.state.dayChosen)
						.subtract(5, "day")
						.toDate(),
				});
			}
		} 
		else if (flipUnit === "day" || flipUnit === "agenda") {
			if (prevOrNext === "NEXT" && _this.state.dayChosen <= maxDateAvailable) {
				_this.setState({
					dayChosen: moment(_this.state.dayChosen)
						.add(1, "day")
						.toDate(),
				});
			} else if (prevOrNext === "PREV") {
				_this.setState({
					dayChosen: moment(_this.state.dayChosen)
						.subtract(1, "day")
						.toDate(),
				});
			}
		}
		
		else if (flipUnit === "TODAY") {
			_this.setState({
				dayChosen: now,
			});
		}
	};
	handleSubmit(event) {
		//this.setState({[event.target.name]:event.target.value})
		event.preventDefault();
		const docRef = this.props.values;
		docRef.set({ ...this.state });
		this.setState({ openForm: false });
	}
	handleChange(event) {
		this.setState({ [event.target.name]: event.target.value });
	}
	handleClose = () => {
		this.setState({ openEventModal: false });
	};
	handleSelect = ({ start, end }) => {
		const date = new Date(start).toISOString().split("T")

		this.setState({
			event: {
				...Event,
				created_by: authRef.currentUser.uid,
				user: authRef.currentUser.uid,
				date: date[0],
				hour:date[1].slice(0,8),
				start: start.getTime(),
				end: end.getTime(),
			},
			openEventModal: true,
		});
		console.log(this.state.event);
	};
	handleSelectEvent = event => {
		console.log(event);
		this.setState({
			event: { ...event, updated_by: authRef.currentUser.uid },
			openEventModal: true,
		});
	};

	render = () => {
		const { classes, defaultView, views, height, struct } = this.props;
		const eventForm = (
			<Modal
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
				open={this.state.openEventModal}
				onClose={this.handleClose}
			>
				<div className={classes.paper}>
					{this.state.event && (
						<form className={classes.container} noValidate>
							<Form
								formName="Cita"
								collection={events}
								struct={struct || appointment}
								itemModel={this.state.event}
								onlyForm
							/>
						</form>
					)}
				</div>
			</Modal>
		);

		return (
			<div>
				<BigCalendar
					selectable
					onSelectEvent={this.handleSelectEvent}
					onSelectSlot={this.handleSelect}
					events={this.state.events}
					defaultDate={new Date()}
					defaultView={defaultView || BigCalendar.Views.WORK_WEEK}
					min={this.state.minTime}
					max={this.state.maxTime}
					localizer={localizer}
					culture="es"
					startAccessor="start"
					endAccessor="end"
					views={views || allViews}
					date={this.state.dayChosen}
					onNavigate={this.onNavigate}
					style={{ height: height || "900px" }}
					step={30}
					timeslots = {1}
				/>
				{eventForm}
			</div>
		);
	};
}

export default withStyles(styles)(Calendar);
