import React from "react";
import Modal from "@material-ui/core/Modal";
import classNames from "classnames";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import TextField from "@material-ui/core/TextField";
import AddIcon from "@material-ui/icons/Add";
import Grid from "@material-ui/core/Grid";
import { db, events, authRef } from "../../config/firebase";
import Form from "../../components/Form";
import { appointment } from "../../config/structs";
import CardEvent from "../../components/CardEvent";
import Event from "../../models/Event";
import Calendar from "./Calendar";
const styles = theme => ({
	modal: {
		top: 50,
		left: 50,
		transform: `translate(-50%, -50%)`,
	},
	fab: {
		position: "absolute",
		bottom: theme.spacing.unit * 2,
		right: theme.spacing.unit * 2,
	},
	paper: {
		position: "absolute",
		width: theme.spacing.unit * 50,
		backgroundColor: theme.palette.background.paper,
		boxShadow: theme.shadows[5],
		padding: theme.spacing.unit * 4,
		outline: "none",
		top: "50%",
		left: "50%",
		transform: `translate(-50%, -50%)`,
	},
});
class Appointment extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			Event: Event,
			date: undefined,
			openForm: false,
			events: [],
		};
	}
	struct = appointment;
	handleInputDate = event => {
		const dateSelected = event.target.value;
		this.setState({ date: dateSelected });
	};
	handleSubmit(event) {
		//this.setState({[event.target.name]:event.target.value})
		event.preventDefault();
		const docRef = this.props.values;
		docRef.set({ ...this.state });
		this.setState({ openForm: false });
	}
	handleChange(event) {
		this.setState({ [event.target.name]: event.target.value });
	}

	handleOpen = () => {
		this.setState({ openForm: true });
	};
	handleClose = () => {
		this.setState({ openForm: false });
	};
	render() {
		const { classes } = this.props;

		const eventForm = (
			<Modal
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
				open={this.state.openForm}
				onClose={this.handleClose}
			>
				<div className={classes.paper}>
					<Grid item xs={12}>
						<TextField
							id="date"
							label="Fecha"
							name="date"
							type="date"
							defaultValue={new Date()}
							className={classes.textField}
							InputLabelProps={{
								shrink: true,
							}}
							onChange={this.handleInputDate}
						/>
					</Grid>
					{this.state.date && (
						<Calendar
							date={this.state.date}
							query={events.where("date", "==", this.state.date)}
							defaultView="day"
							views={["day"]}
							user={authRef.currentUser.uid}
							height="400px"
							struct={this.struct.slice(1)} // to remove search user
						/>
					)}
				</div>
			</Modal>
		);

		return (
			<div>
				<p>Agendar Cita</p>
				{eventForm}
				<Form
					formName="Cita"
					collection={events}
					query={events.where("user", "==", authRef.currentUser.uid)}
					CardItem={CardEvent}
					onlyList
				/>
				<Fab className={classes.fab} color="primary" onClick={this.handleOpen}>
					<AddIcon />
				</Fab>
			</div>
		);
	}
}

export default withStyles(styles)(Appointment);
