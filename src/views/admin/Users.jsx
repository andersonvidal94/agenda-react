import React from "react";
import Modal from "@material-ui/core/Modal";
import classNames from "classnames";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import TextField from "@material-ui/core/TextField";
import AddIcon from "@material-ui/icons/Add";
import SaveIcon from "@material-ui/icons/Save";
import Grid from "@material-ui/core/Grid";
import { db, users, authRef } from "../../config/firebase";
import CardUser from "../../components/CardUser";
import { FormControl, InputLabel, Select, MenuItem } from "@material-ui/core";
import { city, structUser } from "../../config/structs";
import Form from "../../components/Form";
import User from "../../models/User";
import { auth } from "firebase";
const styles = theme => ({
	modal: {
		top: 50,
		left: 50,
		transform: `translate(-50%, -50%)`,
	},
	fab: {
		position: "absolute",
		bottom: theme.spacing.unit * 2,
		right: theme.spacing.unit * 2,
	},
	paper: {
		position: "absolute",
		width: theme.spacing.unit * 50,
		backgroundColor: theme.palette.background.paper,
		boxShadow: theme.shadows[5],
		padding: theme.spacing.unit * 4,
		outline: "none",
		top: "50%",
		left: "50%",
		transform: `translate(-50%, -50%)`,
	},
});

class Users extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			user: {},
			users: [],
			date: undefined,
			openForm: false,
			isNew: true,
			city: "villavicencio",
		};
	}


	createUser (user){
		authRef.createUserWithEmailAndPassword(
			user.email,
			user.password,
			).then(firebaseUser => {
				delete user.password
				users.doc(firebaseUser.user.uid).set(user)
			});
	} 

	render() {
		const { classes } = this.props;
		return (
			<div>
				<Form
					formName="Usuarios"
					collection={users}
					query={users.where("city", "==", this.state.city)}
					struct={structUser}
					CardItem={CardUser}
					onCreated={this.createUser}
					isCrud
					itemModel={User}
				/>
			</div>
		);
	}
}

export default withStyles(styles)(Users);
