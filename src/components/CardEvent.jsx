import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import classNames from "classnames";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import red from "@material-ui/core/colors/red";
import FavoriteIcon from "@material-ui/icons/Favorite";
import EditIcon from "@material-ui/icons/Edit";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { users } from "../config/firebase";

const styles = theme => ({
	card: {
		maxWidth: 400,
	},
	media: {
		height: 0,
		paddingTop: "56.25%", // 16:9
	},
	actions: {
		display: "flex",
	},
	expand: {
		transform: "rotate(0deg)",
		marginLeft: "auto",
		transition: theme.transitions.create("transform", {
			duration: theme.transitions.duration.shortest,
		}),
	},
	expandOpen: {
		transform: "rotate(180deg)",
	},
	avatar: {
		backgroundColor: red[500],
	},
});

class CardEvent extends React.Component {
	state = { expanded: false, user:{} };

	fetchUser(id) {
		users.doc(id).get().then((doc)=>{
			if(doc.exists)
			this.setState({user: doc.data()})
		})
	}

	componentDidMount(){
		this.fetchUser(this.props.item.user)
	}

	render() {
		const { classes, item } = this.props;

		return (
			<Card className={classes.card}>
				<CardHeader
					action={
						<IconButton>
							<MoreVertIcon />
						</IconButton>
					}
					title={this.state.user.name + " " + this.state.user.last_name }
					subheader={item.description}
				/>
				<CardContent>
					<Typography component="p">Fecha: {new Date(item.start).toLocaleString() || ""}</Typography>
					<Typography component="p">
						Ciudad: {item.city || ""}
					</Typography>
				</CardContent>
				<CardActions className={classes.actions} disableActionSpacing>
					<IconButton aria-label="agregar a favoritos">
						<FavoriteIcon />
					</IconButton>
					<Button
						variant="contained"
						size="small"
						className={classes.button}
						onClick={() => this.props.onEdit(item)}
					>
						<EditIcon
							className={classNames(classes.leftIcon, classes.iconSmall)}
						/>
						Editar
					</Button>
				</CardActions>
			</Card>
		);
	}
}

CardEvent.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CardEvent);