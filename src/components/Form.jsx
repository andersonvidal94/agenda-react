import React from "react";
import Modal from "@material-ui/core/Modal";
import classNames from "classnames";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import TextField from "@material-ui/core/TextField";
import AddIcon from "@material-ui/icons/Add";
import SaveIcon from "@material-ui/icons/Save";
import Grid from "@material-ui/core/Grid";

import { connectAutoComplete, InstantSearch } from "react-instantsearch-dom";
import { searchClient } from "../config/algolia";
import CreatableSelect from "react-select/lib/Creatable";
import { FormControl, InputLabel, Select, MenuItem } from "@material-ui/core";
import {Redirect} from "react-router-dom"

const styles = theme => ({
	modal: {
		top: 50,
		left: 50,
		transform: `translate(-50%, -50%)`,
	},
	fab: {
		position: "absolute",
		bottom: theme.spacing.unit * 2,
		right: theme.spacing.unit * 2,
	},
	paper: {
		position: "absolute",
		width: theme.spacing.unit * 50,
		backgroundColor: theme.palette.background.paper,
		boxShadow: theme.shadows[5],
		padding: theme.spacing.unit * 4,
		outline: "none",
		top: "50%",
		left: "50%",
		transform: `translate(-50%, -50%)`,
	},
	formControl: {
		width: "100%",
	},
	search:{
		marginBottom: "1em",
	},
	button:{
		marginTop: "1em",
	}
});

const Autocomplete = ({ hits, currentRefinement, refine, onChange }) => {


	return (	<CreatableSelect
			isClearable
			options={hits}
			onChange={onChange}
			getOptionLabel={data => {
				// "data" contains the current option object, so you just use your "labelKey" to return the value of that property.
				return data["name"] + " " + data["last_name"];
			}}
			getOptionValue={data => {
				// "data" contains the current option object, so you just use your "valueKey" to return the value of that property.
				return data["objectID"];
			}}
			placeholder="Buscar Paciente"
			onCreateOption={(input)=>(<Redirect to="/app/usuarios" state={{openForm: true, user:{name: input }}}></Redirect>)}
		/>
	);
}


const CustomAutocomplete = connectAutoComplete(Autocomplete);
/*props {struct,formName, query, collection, isCrud}*/

class Form extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			item: props.itemModel || {},
			items: [],
			date: undefined,
			openForm: false,
			isNew: true,
			docRef: undefined,
		};
		this.handleEdit = this.handleEdit.bind(this);
	}
	componentDidMount() {
		const { query, isCrud, onlyForm, itemModel, onlyList } = this.props;
		const items = [...this.state.items];
		const self = this;
		if (onlyForm && itemModel) this.setState({ item: itemModel });
		if (isCrud || onlyList)
			query.onSnapshot(snapshot => {
				snapshot.docChanges().forEach(change => {
					const data = { ...change.doc.data(), id: change.doc.id };
					if (change.type === "added") {
						items.push(data);
					}
					if (change.type === "modified") {
						let found = items.findIndex(e => {
							return e.hc === change.doc.hc;
						});
						items[found] = data;
					}
					if (change.type === "removed") {
						let found = items.findIndex(e => {
							return e.hc === change.doc.hc;
						});
						items.splice(found, 1);
					}
				});
				self.setState({ items: items });
			});
	}
	componentWillUnmount() {
		if (this.props.isCrud) {
			const unsuscribe = this.props.query.onSnapshot(() => {});
			unsuscribe();
		}
	}
	handleEdit(element) {
		console.log(element);
		const collection = this.props.collection;
		this.setState({
			docRef: collection.doc(element.id),
			item: element,
			openForm: true,
		});
	}
	handleSubmit = event => {
		const { onCreated, collection } = this.props;
		//this.setState({[event.target.name]:event.target.value})
		event.preventDefault();
		if (onCreated) {
			onCreated(this.state.item);
		} else if (this.docRef) {
			this.docRef.set(this.state.item);
		} else {
			collection.add(this.state.item);
		}
		this.setState({ item: {}, openForm: false });
	};
	handleUserChange = user => {
		const item = {
			...this.state.item,
			user: user.objectID,
			title: user.name + " " + user.last_name,
		};
		this.setState({
			item: item,
		});
	};
	handleChange = event => {
		const item = {
			...this.state.item,
			[event.target.name]: event.target.value,
		};
		this.setState({
			item: item,
		});
	};
	handleOpen = () => {
		this.setState({ item: { ...this.props.itemModel }, openForm: true });
	};
	handleClose = () => {
		this.setState({ openForm: false });
	};

	render() {
		const {
			classes,
			formName,
			isCrud,
			CardItem,
			struct,
			onlyForm,
			onlyList,
		} = this.props;

		const itemForm = (this.state.item && !onlyList) && (
			<div xs={12} md={12} className={classes.paper}>
				{struct.map(e => {
					if (e.type === "select")
						return (
							<FormControl className={classes.formControl} key={e.key}>
								<InputLabel shrink htmlFor={e.key}>
									{e.label}
								</InputLabel>
								<Select
									value={this.state.item[e.key]}
									onChange={this.handleChange}
									inputProps={{
										name: e.key,
										id: e.key,
										...e.inputProps,
									}}
								>
									{e.options.map((opt, key) => (
										<MenuItem key={key} value={opt.value}>
											{opt.text}
										</MenuItem>
									))}
								</Select>
							</FormControl>
						);
					else if (e.type === "autoComplete") {
						return (
							<div className={classes.search}>
							<InstantSearch indexName="user" searchClient={searchClient}>
								<CustomAutocomplete
									onChange={option => {
										this.handleUserChange(option);
									}}
								/>
							</InstantSearch>
							</div>
						);
					} else
						return (
							<TextField
								key={e.key}
								label={e.label}
								value={this.state.item[e.key]}
								name={e.key}
								type={e.type}
								className={classes.textField}
								onChange={this.handleChange}
								{...e.inputProps}
							/>
						);
				})}
				<Button
					variant="contained"
					size="small"
					className={classes.button}
					onClick={this.handleSubmit}
				>
					<SaveIcon
						className={classNames(classes.leftIcon, classes.iconSmall)}
					/>
					Guardar
				</Button>
			</div>
		);

		return (
			<div>
				<h1>{formName}</h1>
				{isCrud && (
					<div>
						<Modal
							aria-labelledby="simple-modal-title"
							aria-describedby="simple-modal-description"
							open={this.state.openForm}
							onClose={this.handleClose}
						>
							{this.state.item && itemForm}
						</Modal>

						{this.state.items.length > 0 &&
							this.state.items.map((item, key) => (
								<CardItem item={item} key={key} onEdit={this.handleEdit} />
							))}
						<Fab
							className={classes.fab}
							color="primary"
							onClick={this.handleOpen}
						>
							<AddIcon />
						</Fab>
					</div>
				)}
				{onlyForm && itemForm}

				{onlyList &&
					this.state.items.length > 0 &&
					this.state.items.map((item, key) => (
						<CardItem item={item} key={key} onEdit={this.handleEdit} />
					))}
			</div>
		);
	}
}

export default withStyles(styles)(Form);
