import React from "react";
import DashboardIcon from '@material-ui/icons/Dashboard';
import Person from '@material-ui/icons/Person';
import CalendarIcon from '@material-ui/icons/CalendarViewDay';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import Calendar from "../views/admin/Calendar";
import Profile from '../views/admin/Profile'
import Appointment from "../views/admin/Appointment.jsx";
import Users from "../views/admin/Users";
export const dashboardRoutes = [
    {
        path: '/app/agenda',
        sidebarName: 'Agenda',
        navbarName: 'Agenda',
        icon: CalendarTodayIcon,
        component: Calendar,
    },
    {
        path: '/app/cita',
        sidebarName: 'Citas',
        navbarName: 'Citas',
        icon: CalendarIcon,
        component: Appointment,
    },
    {
        path: '/app/perfil',
        sidebarName: 'Perfil',
        navbarName: 'Perfil',
        icon: Person,
        component: Profile,
    },
    {
        path: '/app/usuarios',
        sidebarName: 'Usuarios',
        navbarName: 'Perfil',
        icon: Person,
        component: Users,
    },
  
];

