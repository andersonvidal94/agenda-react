import Dashboard from '../layouts/Dashboard.jsx';
//TODO import Register from '../views/auth/Register.js';
import Login from '../views/auth/Login.js';

const indexRoutes = [
   // { path: '/register', component: Register },
    { path: '/login', component: Login },
    { path: '/app', component: Dashboard, isPrivate: true },
];

export default indexRoutes;