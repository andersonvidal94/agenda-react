import React, { Component } from 'react';
import './App.css';
import { Router, Route, Switch, Redirect } from 'react-router';
import PrivateRoute from './routes/PrivateRoute';
import { createBrowserHistory } from 'history';
import indexRoutes from './routes/indexRoutes'
export const hist = createBrowserHistory();

class App extends Component {
  render() {
    return (
         <Router history={hist}>
          <Switch>
              {indexRoutes.map((prop, key) =>
                  prop.isPrivate ? (
                      <PrivateRoute
                          key={key}
                          path={prop.path}
                          component={prop.component}
                      />
                  ) : (
                      <Route key={key} path={prop.path} component={prop.component} />
                  ),
              )}
              <Redirect from="/" to="/login" />
          </Switch>
      </Router>
    );
  }
}

export default App;
