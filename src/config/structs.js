import { cities, authRef } from "./firebase";

export const city ={
    key: "city",
    label: "Ciudad",
    type: "select",
    options: [
        { text: "Villavicencio", value:"villavicencio" },
        { text: "Bogotá", value: "bogota" },
    ],
    inputProps: { required: true },
}

export const structUser =  [
    {
        key: "hc",
        label: "Historia Clinica",
        type: "text",
        inputProps: { required: true },
    },
    {
        key: "name",
        label: "Nombres",
        type: "text",
        inputProps: { required: true },
    },
    {
        key: "last_name",
        label: "Apellidos",
        type: "text",
        inputProps: { required: false },
    },
    {
        key: "vat",
        label: "Documento",
        type: "number",
        inputProps: { required: false },
    },
    {
        key: "address",
        label: "Dirección",
        type: "text",
        inputProps: { required: false },
    },
    city,
    {
        key: "email",
        label: "Correo",
        type: "email",
        inputProps: { required: true },
    },
    {
        key: "phone",
        label: "telefono",
        type: "phone",
        inputProps: { required: true },
    },
    {
        key: "password",
        label: "Contraseña",
        type: "password",
        inputProps: { required: true },
    },
]

export const appointment = [
	{
		key: "user",
		label: "Usuario",
		type: "autoComplete",
        inputProps: { required: true},

	},
	{
		key: "date",
		label: "Fecha",
		type: "date",
		inputProps: {required: true, disabled: true,}
    },
    {
        key:"hour",
        label:"Hora",
        type:"time",
        inputProps:{disabled: true, }
    },
	{
		key: "description",
		label: "Descripción",
		type: "text",
		inputProps: {
            required: false,
            fullWidth: true,
		}	
	},
	city
 ]