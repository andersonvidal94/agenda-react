import firebase from 'firebase';
import configKeys from './configFirebase';

firebase.initializeApp(configKeys);

export default firebase;

export const authRef = firebase.auth();
export const db = firebase.firestore();
export const events = db.collection("appointments")
export const users = db.collection("users")
export const cities = db.collection("cities")